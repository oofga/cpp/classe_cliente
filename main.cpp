#include <iostream>
#include "cliente.hpp"
#include <string>


void configura_cliente(Cliente *cliente, string nome, long cpf, string email, string telefone, string endereco, string genero){
    cliente->set_nome(nome);
    cliente->set_cpf(cpf);
    cliente->set_email(email);
    cliente->set_telefone(telefone);
    cliente->set_endereco(endereco);
    cliente->set_genero(genero);
}

int main(int argc, char ** argv){

    Cliente cliente1;
    Cliente cliente2;

    configura_cliente(&cliente1, "João", 23423423451,"joão@email.com","5555-4444", "Rua 1, Casa 2", "Masculino");

    configura_cliente(&cliente2, "Maria", 12345676512, "maria@email.com", "1234-0987", "Rua 4, Casa 5", "Feminino");


    //cliente1.set_nome("João");
    //cliente1.set_cpf(23422342343);
    //cliente1.set_email("joao@email.com");
    //cliente1.set_telefone("5555-4444");
    //cliente1.set_endereco("Rua 1, Casa 2");
    //cliente1.set_genero("Masculino");
    
    cliente1.ligar();

    cliente1.imprime_dados();

    cliente2.ligar();

    cliente2.imprime_dados();
    /*
    cout << "Nome: " << cliente1.get_nome() << endl;
    cout << "CPF: " << cliente1.get_cpf() << endl;
    cout << "Telefone: " << cliente1.get_telefone() << endl;
    cout << "Email: " << cliente1.get_email() << endl;
    cout << "Endereco: " << cliente1.get_endereco() << endl;
    cout << "Gênero: " << cliente1.get_genero() << endl;
    */
    return 0;
}








