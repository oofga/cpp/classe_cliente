#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <string>

using namespace std;

class Cliente {

private:

    // Atributos
    string nome;
    long cpf;
    string email;
    string telefone;
    string endereco;
    string genero;
public:
    // Métodos
    Cliente();   // Método Construtor (Instancia o objeto)
    ~Cliente();  // Método Destrutor (Destrói o objeto)

    // Métodos Acessores
    string get_nome();
    void set_nome(string nome);
    long get_cpf();
    void set_cpf(long cpf);
    string get_email();
    void set_email(string email);
    string get_telefone();
    void set_telefone(string telefone);
    string get_endereco();
    void set_endereco(string endereco);
    string get_genero();
    void set_genero(string genero);

    // Outros Métodos
    void ligar();
    void imprime_dados();
};

#endif









